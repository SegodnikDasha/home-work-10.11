 В данном репозитории содержаться примеры чек-листов для демонстрационного **интернет-магазина Swag Labs**, который находится по адресу: https://www.saucedemo.com/. 
 
Тест-план: https://docs.google.com/document/d/1SfmfBwY4l-b7uP174nUPehp0l5hao4J-8cp260Y0axg/edit?usp=sharing
 
 Примеры чек-листов и тест-кейсов находятся по адресу: https://docs.google.com/document/d/1KK2JG8mBn1XSfBVivn6NYyXylbZfr6nmjhY2p24iSdU/edit?usp=sharing. 

 Варианты баг-репортов находятся по адресу: https://docs.google.com/document/d/1vRglOlhCdb9Ggj-FUeYcvSiogCZlG0dCv2qqOukbOcw/edit?usp=sharing.

